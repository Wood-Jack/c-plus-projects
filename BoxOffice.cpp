#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

/********
*Program Name: BoxOffice.cpp

*Programmer Name: Woodrow Jackson

*Description: The program calcuates gross and netbox worth

********************/


int main()
{
	double gross;
	double officeProfit;
	double distributor;
	string movieName;
	double adultTicket;
	double childTicket;
	const double adultPrice = 10.00;
	const double childPrice = 6.00;
	const double PERCENT_PROFIT = .20;

	cout << "What is the name of the movie you wish to see?";;
	getline(cin,movieName);

	cout << "How many adult tickets were sold? ";
	cin >> adultTicket;
	cin.ignore();

	cout << "How many child tickets were sold? ";
	cin >> childTicket;
	cin.ignore();

	adultTicket *= adultPrice;
	childTicket *= childPrice;
	gross = (adultTicket + childTicket);
	officeProfit = (gross * PERCENT_PROFIT);
	distributor = gross - officeProfit;

	cout << setprecision(2) << showpoint << fixed;
	cout << "Movie Name: " << setw(35) << movieName << endl;
	cout << "Adult Tickets Sold: " << setw(25) << adultTicket << endl;
	cout << "Child Tickets Sold: " << setw(25) << childTicket << endl;
	cout << "Gross Box Office Profit: " << setw(15) <<  "$" << gross << endl;
	cout << "Net Box Office Profit: " << setw(17) << "$" << officeProfit << endl;
	cout << "Amount Paid to Distributor: " << setw(12) << "$" << distributor << endl;

return 0;
}
