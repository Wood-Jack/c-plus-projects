#include <iostream>
using namespace std;


/********
*Program Name: Restaurant.cpp

*Programmer Name: Woodrow Jackson

*Description: This is a program that calcualte tax and tip on a 
restaurant bill.

********************/

int main()
{
	//Declared Variables
	double mealCost,mealCharge, taxAmount, tipAmount;
	double totalBill;
	 //intialized variables
	mealCharge= 44.50;
	taxAmount= 0.075;
	tipAmount= 0.15;
	//calucations of variable to totalBill variable
	totalBill = (tipAmount * mealCharge* taxAmount) * 100;
	//Displays mealCharge, tipAmount,taxAmount, and totalBill to console window
	cout<<"Meal Charge"<<"$"<<mealCharge<<"\n";
	
	cout<<"Tax Amount"<<taxAmount<<"% \n";
	
	cout<<"Tip Amount"<<tipAmount<<"% \n";
	
	cout<< "Total bill"<<"$"<<totalBill<<"\n";
	return 0;
}
